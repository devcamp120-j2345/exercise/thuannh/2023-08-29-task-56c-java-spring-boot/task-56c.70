package com.devcamp.animalapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


import com.devcamp.animalapi.model.Cat;
import com.devcamp.animalapi.model.Dog;
import com.devcamp.animalapi.service.AnimalService;

@RestController
public class AnimalController {
    @Autowired
    AnimalService animalService;
    
    @GetMapping("/cats")
    public ArrayList<Cat> getAllCatsApi(){
        return animalService.getAllCats();
    }
    @GetMapping("/dogs")
    public ArrayList<Dog> getAllDogsApi(){
        return animalService.getAllDogs();
    }

}
