package com.devcamp.animalapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.animalapi.model.Animal;
import com.devcamp.animalapi.model.Cat;
import com.devcamp.animalapi.model.Dog;

@Service
public class AnimalService {
    Cat cat1 = new Cat("Angel");
    Cat cat2 = new Cat("Moon");
    Cat cat3 = new Cat("Huhu");
    Dog dog1 = new Dog("Becky");
    Dog dog2 = new Dog("Lu");
    Dog dog3 = new Dog("Hihi");

    public ArrayList<Cat> getAllCats(){
        ArrayList<Cat> catList = new ArrayList<>();
        catList.add(cat1);
        catList.add(cat2);
        catList.add(cat3);
        return catList;
    }
    public ArrayList<Dog> getAllDogs(){
        ArrayList<Dog> dogList = new ArrayList<>();
        dogList.add(dog1);
        dogList.add(dog2);
        dogList.add(dog3);
        return dogList;

    }
}
